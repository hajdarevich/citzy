const gulp = require('gulp');
const gzip = require('gulp-gzip');

gulp.task('compress', function() {

  const src = `./dist`;

  gulp.src([`${src}/*.js`, `${src}/*.css`])
    .pipe(gzip())
    .pipe(gulp.dest(`${src}`));
});
