<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ Import namespace="System" %>
<%@ Import namespace="System.Web.UI.WebControls" %>
<%@ Import namespace="System.Web.Security" %>

<script runat=server>
protected void Button1_Click(object sender, EventArgs e)
{
	if (FormsAuthentication.Authenticate(txtUsername.Value, txtPassword.Value))
	{
		lblStatus.Text = ("Welcome " + txtUsername.Value);
		FormsAuthentication.RedirectFromLoginPage(txtUsername.Value, true);
	}
	else
	{
		lblStatus.Text = "Invalid login!";
	}
}

protected void Button2_Click(object sender, EventArgs e)
{
	FormsAuthentication.SignOut();
	Response.Redirect("index.html", true);
}
</script>
<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:300);

.login-page {
  width: 360px;
  padding: 8% 0 0;
  margin: auto;
}
.form {
  position: relative;
  z-index: 1;
  background: #FFFFFF;
  max-width: 360px;
  margin: 0 auto 100px;
  padding: 45px;
  text-align: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}
.form input {
  font-family: "Roboto", sans-serif;
  outline: 0;
  background: #f2f2f2;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
}
.form input[type="submit"] {
  font-family: "Roboto", sans-serif;
  text-transform: uppercase;
  outline: 0;
  background: #4CAF50;
  width: 100%;
  border: 0;
  padding: 15px;
  color: #FFFFFF;
  font-size: 14px;
  -webkit-transition: all 0.3 ease;
  transition: all 0.3 ease;
  cursor: pointer;
}
.form input[type="submit"]:hover,.form input[type="submit"]:active,.form input[type="submit"]:focus {
  background: #43A047;
}
.form .message {
  margin: 15px 0 0;
  color: #b3b3b3;
  font-size: 12px;
}
.form .message a {
  color: #4CAF50;
  text-decoration: none;
}
.container {
  position: relative;
  z-index: 1;
  max-width: 300px;
  margin: 0 auto;
}
.container:before, .container:after {
  content: "";
  display: block;
  clear: both;
}
body {
  background: #76b852; /* fallback for old browsers */
  background: -webkit-linear-gradient(right, #76b852, #8DC26F);
  background: -moz-linear-gradient(right, #76b852, #8DC26F);
  background: -o-linear-gradient(right, #76b852, #8DC26F);
  background: linear-gradient(to left, #76b852, #8DC26F);
  font-family: "Roboto", sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;      
}
</style>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login Form</title>
</head>
<body>
    <div class="login-page">
        <div class="form">
            <form id="form1" runat="server">
				<input ID="txtUsername" type="text" placeholder="username" runat="server" />
                <input ID="txtPassword" type="password" placeholder="password" runat="server" />
				<asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="login" />
				<asp:LinkButton ID="Button2" runat="server" onclick="Button2_Click" Text="logout" />  
				<p class="message"><asp:Label ID="lblStatus" runat="server" Text="Please login" /></p>
            </form>
        </div>
    </div>
</body>
</html>