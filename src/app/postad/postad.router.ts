import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { PostAdComponent } from './postad.component';
import { CreateAdComponent } from './create/create.component';

const routes: Route[] = [
  {
    path: '',
    component: PostAdComponent,
    children: [{
      path: ':postad',
      component: CreateAdComponent
    }]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
