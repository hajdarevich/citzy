import { Injectable } from '@angular/core';

@Injectable()
export class PostAdService {

  /**
   * Helper function for setting correct path to end category.
   *
   * @param dataSrc
   * @param index
   * @param markForSelectionPath
   */
  computePathForSelection<T>(dataSrc: T, index: number, markForSelectionPath: T[]): T[] {
    markForSelectionPath =  markForSelectionPath.slice(0, index);
    markForSelectionPath.push(dataSrc);

    return markForSelectionPath;
  }
}
