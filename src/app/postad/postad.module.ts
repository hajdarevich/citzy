import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PostAdComponent } from './postad.component';
import { routing } from './postad.router';
import { SharedModule } from '../shared/shared.module';
import { CreateAdComponent } from './create/create.component';
import { CreateAdSelectCategoryComponent } from './create/category/category.component';
import { CreateAdSelectLocationComponent } from './create/location/location.component';
import { PasswordStrengthAdComponent } from './create/password/password.component';
import { CreateAdUploadImageComponent } from './create/images/images.component';
import { CreateAdUploadImageButtonComponent } from './create/images/button/button.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ],
  declarations: [
    PostAdComponent,
    PasswordStrengthAdComponent,
    CreateAdUploadImageComponent,
    CreateAdUploadImageButtonComponent,
    CreateAdSelectCategoryComponent,
    CreateAdSelectLocationComponent,
    CreateAdComponent
  ],
  bootstrap: [
    PostAdComponent
  ]
})
export class PostAdModule {}
