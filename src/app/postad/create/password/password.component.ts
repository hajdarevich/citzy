import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-postad-password-strength',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.less']
})
export class PasswordStrengthAdComponent {

  strength: number;

  @Input() set input(data: string) {
    if (data) {
      this.strength = 0;
      this.strength = this.checkValidity(data);
    }
  }

  checkValidity(state: string) {

    let innerState = 0;

    if (!state) {
      return 0;
    }

    if (state.length > 5) {
      innerState += 1;
    }

    if (state.match(/[^\w\s]|[a-zA-Z]/)) {
      innerState += 1;
    }

    if (state.match(/\d/)) {
      innerState += 1;
    }

    if (state.match(/[-!$%^&@*#-()_+|~=`{}\[\]:";'<>?,.\/]/)) {
      innerState += 1;
    }

    return innerState;
  }

}
