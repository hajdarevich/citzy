import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { ILocation } from '../../../store/layout/layout.reducer';
import { Iad } from '../../../store/postad/postad.reducer';
import { IAppState } from '../../../store/index';
import {
  GET_LOCATIONS_BY_PARENT, REST_LOCATIONS,
  SET_LOCATION_FOR_SELECTION
} from '../../../store/postad/postad.actions';
import { PostAdService } from '../../postad.service';

@Component({
  selector: 'app-postad-create-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.less'],
  providers: [PostAdService]
})
export class CreateAdSelectLocationComponent implements OnInit {

  ad$: Observable<Iad>;
  locations$: Observable<ILocation[]>;

  selectedBrowsLocation: ILocation;
  markForSelectionPath: ILocation[] = [];
  markAsSelection: ILocation;

  @Output('onLocationSelected') onLocationSelected: EventEmitter<ILocation> = new EventEmitter<ILocation>();
  @Input() visibility: boolean;

  constructor(private store: Store<IAppState>, private postAdService: PostAdService) {}

  ngOnInit() {
    this.ad$ = this.store.select('postad');
    this.locations$ = this.store.select('layout').pluck('locations');
  }

  browsLocationSelect(location: ILocation) {
    this.selectedBrowsLocation = location;

    delete this.markAsSelection;
    this.markForSelectionPath = [];
    this.markForSelectionPath.push(location);

    this.store.dispatch({
      type: REST_LOCATIONS
    });

    this.store.dispatch({
      type: GET_LOCATIONS_BY_PARENT,
      payload: {
        locationID: location.id,
        index: 0
      }
    });

  }

  nextOrCheck(location: ILocation, index: number) {
    this.markForSelectionPath = this.postAdService.computePathForSelection<ILocation>(location, index, this.markForSelectionPath);

    if (location.hasChildrens) {

      this.store.dispatch({
        type: GET_LOCATIONS_BY_PARENT,
        payload: { locationID: location.id, index }
      });

    } else {

      this.store.dispatch({
        type: SET_LOCATION_FOR_SELECTION,
        payload: {
          locationID: location.id,
          index: index
        }
      });

      this.markAsSelection = Object.assign({}, location, {
        path: this.markForSelectionPath
      });
    }
  }
}
