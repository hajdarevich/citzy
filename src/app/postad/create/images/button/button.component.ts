import { Component, ChangeDetectionStrategy, Output, EventEmitter, HostListener } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-postad-upload-image-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.less']
})
export class CreateAdUploadImageButtonComponent {

  @Output('onFileSelected') onFileSelected: EventEmitter<File> = new EventEmitter<File>();

  hover: boolean;

  selectFile(data) {
    const [ file ] = data.files;
    this.onFileSelected.next(file);
  }

  @HostListener('drop', ['$event'])
  onDrop(event: Event & { dataTransfer?: { files: FileList } }) {
    event.preventDefault();
    event.stopPropagation();

    this.selectFile(event.dataTransfer);
    this.hover = false;
  }

  @HostListener('dragover', ['$event'])
  onDraOver(event: Event) {
    event.preventDefault();
    event.stopPropagation();

    if (!this.hover) {
      this.hover = true;
    }
  }

  @HostListener('dragleave', ['$event'])
  onDraLeave(event: Event) {
    event.preventDefault();
    event.stopPropagation();

    if (this.hover) {
      this.hover = false;
    }
  }
}
