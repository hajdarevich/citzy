import { Component, ChangeDetectionStrategy, ElementRef, ViewChild, Output, EventEmitter, Input } from '@angular/core';

declare var Sortable: any;

interface IUploadedImage {
  data: string;
  name: string;
  uploading: boolean;
}

@Component({
  changeDetection: ChangeDetectionStrategy.Default,
  selector: 'app-postad-upload-image',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.less']
})
export class CreateAdUploadImageComponent {
  @ViewChild('cards') cards: ElementRef;

  images: IUploadedImage[] = [];
  hasError = false;

  @Input() limit: number = 9;

  @Output('onImageUpload') onImageUpload: EventEmitter<string> = new EventEmitter<string>();
  @Output('onImageRemove') onImageRemove: EventEmitter<string> = new EventEmitter<string>();

  onFileSelected(file: File) {
    if (!file) {
      return;
    }

    if (this.checkExtension(file.type) < 0 || this.checkSize(file.size)) {
      this.hasError = true;
      return;
    }

    this.hasError = false;

    const reader  = new FileReader();
    const component = this;

    reader.onloadend = () => {
      const image = {
        data: reader.result,
        name: file.name,
        uploading: true
      };

      component.images.push(image);

      setTimeout(() => {
        const index = component.images.indexOf(image);
        component.images[index] = Object.assign(image, {
          uploading: false
        });
        this.onImageUpload.next(component.images[index].name);
      }, 1500);

      const sortable = Sortable.create(this.cards.nativeElement, {
        filter: '.ignore-elements'
      });

    };

    reader.readAsDataURL(file);
  }

  removeImage(index: number) {
    const [ image ] = this.images.splice(index, 1);
    this.onImageRemove.next(image.name);
  }

  checkExtension(type: string): number {
    return ['image/png', 'image/jpeg', 'image/gif', 'image/bmp'].indexOf(type);
  }

  checkSize(size: number): boolean {
    return size >  2 * 1024 * 1024;
  }
}

