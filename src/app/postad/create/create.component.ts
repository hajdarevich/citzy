import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { IAppState } from '../../store/index';
import {
  CREATE_AD_SEARCH_CATEGORY, CREATE_AD_SELECT_CATEGORY,
  CREATE_AD_CLEAR_CATEGORY, CREATE_AD_SEARCH_LOCATION, CREATE_AD_SELECT_LOCATION,
  CREATE_AD_CLEAR_LOCATION, SET_SELLER_TYPE, SET_TITLE, SET_PRICE,
  SET_DESCRIPTION, SET_INCLUDE_LINK, SET_WEBSITE, GET_SUBCATEGORIES_BY_PARENT_RESET, GET_SUBCATEGORIES_BY_PARENT,
  SET_PROMOTION, SET_YOUTUBE, LOAD_AD, SET_IMAGE, REMOVE_IMAGE
} from '../../store/postad/postad.actions';
import { Iad, ILocation } from '../../store/postad/postad.reducer';
import { ICategory, IAttributes } from '../../store/layout/layout.reducer';

@Component({
  selector: 'app-postad-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.less']
})
export class CreateAdComponent implements OnInit {

  ad$: Observable<Iad>;
  adForm: FormGroup;

  isBrowseCategoryOpen = false;
  isBrowseLocationOpen = false;

  @ViewChild('passwordInput') passwordInput: ElementRef;

  constructor(private store: Store<IAppState>, private fb: FormBuilder, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.ad$ = this.store.select('postad');

    this.store.dispatch({
      type: LOAD_AD,
      payload: {
        id: this.activatedRoute.snapshot.params['postad']
      }
    });

    this.adForm = this.fb.group({
      'category': this.fb.group({
        'categorySearch': []
      }),
      'location': this.fb.group({
        'locationSearch': []
      }),
      'sellerType': this.fb.group(({
        'type': []
      })),
      'title': this.fb.group(({
        'value': ['', [Validators.required, Validators.minLength(1)]],
        'urgent': []
      })),
      'description': this.fb.group(({
        'value': ['', [Validators.required, Validators.minLength(20)]]
      })),
      'price': this.fb.group(({
        'value': ['', [Validators.required, Validators.pattern(/^[0-9]{1,10}$/)]]
      })),
      'weblink': this.fb.group(({
        'value': ['http://', Validators.pattern(/(http|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/)],
        'include': ['']
      })),
      'images': this.fb.group(({
        'value': [''],
        'youtube': ['']
      })),
      'promotion': this.fb.group(({
        'urgent': [false],
        'urgent_price': [9.95],
        'featured': [false],
        'featured_price': [15],
        'spotlight': [false],
        'spotlight_price': [28.95],
        'all': [false]
      })),
      'user': this.fb.group(({
        'firstName': ['', [Validators.required]],
        'lastName': ['', [Validators.required]],
        'email': ['', [Validators.required, Validators.pattern(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)]],
        'password': ['', [Validators.required, Validators.pattern(/(?=.*[$@$!%*#?&])[\p{L}\d$@$!%*#?&]{6,}$/)]],
        'telephone': [''],
        'contact': this.fb.group({
          'email': [''],
          'phone': [''],
        }, {
          validator: this.contactValidation
        }),
        'receive_news': [true]
      })),
    });

    this.mutateForm();

    const generalDebounceTime = 400;

    // perform category search
    this.adForm.get('category').get('categorySearch')
      .valueChanges
      .debounceTime(generalDebounceTime)
      .filter((searchTerm: string) => searchTerm && searchTerm.length > 2)
      .subscribe((searchTerm: string) => {
        this.store.dispatch({
          type: CREATE_AD_SEARCH_CATEGORY,
          payload: searchTerm
        });
      });

    // perform title populate
    this.adForm.get('title').get('value')
      .valueChanges
      .debounceTime(generalDebounceTime)
      .subscribe((titleValue: string) => {
        this.store.dispatch({
          type: SET_TITLE,
          payload: titleValue
        });
      });

    // update title urgent if proption urgent is clicked
    this.adForm.get('promotion').get('urgent')
      .valueChanges
      .subscribe((isUrgent: boolean) => {
        this.adForm.get('title').get('urgent').setValue(isUrgent);
      });

    // perform description populate,
    this.adForm.get('description').get('value')
      .valueChanges
      .debounceTime(generalDebounceTime)
      .filter((value: string) => this.adForm.get('description').get('value').valid)
      .subscribe((descriptionValue: string) => {
        this.store.dispatch({
          type: SET_DESCRIPTION,
          payload: descriptionValue
        });
      });

    // perform price populate
    this.adForm.get('price').get('value')
      .valueChanges
      .debounceTime(generalDebounceTime)
      .subscribe((priceValue: number) => {
        this.store.dispatch({
          type: SET_PRICE,
          payload: priceValue
        });
      });

    // perform weblink populate
    this.adForm.get('weblink').get('value')
      .valueChanges
      .debounceTime(generalDebounceTime)
      .subscribe((webLink: string) => {
        this.store.dispatch({
          type: SET_WEBSITE,
          payload: {
              link: webLink,
            include: this.adForm.get('weblink').get('include').value
          }
        });
      });

    // perform youtube populate
    this.adForm.get('images').get('youtube')
      .valueChanges
      .debounceTime(generalDebounceTime)
      .subscribe((youtubeLink: string) => {
        this.store.dispatch({
          type: SET_YOUTUBE,
          payload: youtubeLink
        });
      });
  }

  searchForLocation(searchTerm: string) {
    this.store.dispatch({
      type: CREATE_AD_SEARCH_LOCATION,
      payload: searchTerm
    });
  }

  /**
   * Select category for new ad.
   *
   * @param category
   * @param recreateTree
   */
  selectCategory(category: ICategory, recreateTree = false) {
    this.isBrowseCategoryOpen = false;

    this.store.dispatch({
      type: CREATE_AD_SELECT_CATEGORY,
      payload: {
        id: category.id,
        displayName: category.displayName,
        path: category.path
      }
    });

    if (recreateTree) {

      /*

      Recreate tree when category is selected from search results.

      this.store.dispatch({
        type: GET_SUBCATEGORIES_BY_PARENT_RESET
      });

      const path = category.path.slice(0, -1);

      path.map((item: ICategory, index: number) => {

        this.store.dispatch({
          type: GET_SUBCATEGORIES_BY_PARENT,
          payload: { categoryID: item.id, index }
        });

      });
      */
    }
  }

  /**
   * Select location for new ad.
   *
   * @param location
   */
  selectLocation(location: ILocation) {
    this.isBrowseLocationOpen = false;

    this.store.dispatch({
      type: CREATE_AD_SELECT_LOCATION,
      payload: location
    });
  }

  changeSellerType(sellerType: string) {
    this.store.dispatch({
      type: SET_SELLER_TYPE,
      payload: sellerType
    });
  }

  isUrgent(isUrgent: boolean) {
    this.adForm.get('promotion').get('urgent').setValue(isUrgent);
    this.selectPromotion();
  }

  includeLink(includeLink: boolean) {
    this.store.dispatch({
      type: SET_INCLUDE_LINK,
      payload: includeLink
    });
  }

  clearCategorySelection() {

    this.isBrowseCategoryOpen = true;

    this.adForm.get('category').get('categorySearch').reset();

    this.store.dispatch({
      type: CREATE_AD_CLEAR_CATEGORY
    });
  }

  clearLocationSelection() {
    this.adForm.get('location').get('locationSearch').reset();

    this.isBrowseLocationOpen = true;

    this.store.dispatch({
      type: CREATE_AD_CLEAR_LOCATION
    });
  }

  browseCategory() {
    this.isBrowseCategoryOpen = !this.isBrowseCategoryOpen;
  }

  browseLocation() {
    this.isBrowseLocationOpen = !this.isBrowseLocationOpen;
  }

  selectAllPromotion(setAllTo: boolean) {
    this.adForm.get('promotion').get('urgent').setValue(setAllTo);
    this.adForm.get('promotion').get('featured').setValue(setAllTo);
    this.adForm.get('promotion').get('spotlight').setValue(setAllTo);

    this.store.dispatch({
      type: SET_PROMOTION,
      payload: this.adForm.get('promotion').value
    });
  }

  onImageUpload(name: string) {
    this.store.dispatch({
      type: SET_IMAGE,
      payload: name
    });
  }

  onImageRemove(name: string) {
    this.store.dispatch({
      type: REMOVE_IMAGE,
      payload: name
    });
  }

  selectPromotion() {
    this.store.dispatch({
      type: SET_PROMOTION,
      payload: this.adForm.get('promotion').value
    });
  }

  showPassword() {
    if (this.passwordInput.nativeElement.type === 'password') {
      this.passwordInput.nativeElement.setAttribute('type', 'text');
    } else {
      this.passwordInput.nativeElement.setAttribute('type', 'password');
    }
  }

  /**
   * Main function for handling Post an Ad form.
   */
  postAd() {

    this.markAllTouched(this.adForm);

    if (this.adForm.valid) {
      console.log('valid');
    } else {
      console.log('not valid');
    }
  }

  mutateForm() {

    // set required mark on sellerType if it exists on category
    this.ad$
      .pluck('category')
      .filter((item: ICategory) => item && item.hasOwnProperty('attributes'))
      .map((item: ICategory) => item.attributes)
      .subscribe((attributes: IAttributes) => {

        if (attributes.hasOwnProperty('sellerType')) {
          this.adForm.get('sellerType').get('type').setValidators(Validators.required);
        } else {
          this.adForm.get('sellerType').get('type').clearValidators();
        }
      });
  }

  /**
   * Validate all fields
   *
   * @param form
   */
  markAllTouched(form: FormGroup) {

    for (let i in form.controls) {

      if (form.controls[i].hasOwnProperty('controls')) {

        form.controls[i].markAsTouched();

        for (let j in form.controls[i]['controls']) {

          if (form.controls[i]['controls'].hasOwnProperty(j)) {
            form.controls[i]['controls'][j].markAsTouched();
          }
        }
      }
    }
  }

  /**
   * User must select one option for contacting.
   *
   * @param formGroup
   * @returns {{required: boolean}}
   */
  contactValidation(formGroup: FormGroup): {}|null {

    let selected: boolean = false;

    for (let control in formGroup.controls) {

      if (formGroup.controls[control].value) {
        selected = true;
      }
    }

    return (selected) ? null : { required: true };
  }

  /**
   * Pretty print of category and location path value.
   *
   * @param dataTree
   * @param sign
   * @param boldLast
   * @returns {string}
   */
  prettyTreePrint(dataTree: ICategory[], sign: string, boldLast: boolean) {
    const explodedArray: string[] = dataTree.map((item: ICategory) => item.displayName);

    if (explodedArray && explodedArray.length && boldLast) {
      explodedArray[explodedArray.length - 1] = `<b>${explodedArray[explodedArray.length - 1]}</b>`;
    }

    return explodedArray.join(` ${sign} `);
  }
}
