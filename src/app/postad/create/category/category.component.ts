import {
  Component, OnInit, EventEmitter, Output, Input
} from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { ICategory } from '../../../store/layout/layout.reducer';
import { Iad } from '../../../store/postad/postad.reducer';
import { IAppState } from '../../../store/index';
import {
  GET_SUBCATEGORIES_BY_PARENT_RESET, GET_SUBCATEGORIES_BY_PARENT,
  SET_CATEGORY_FOR_SELECTION
} from '../../../store/postad/postad.actions';
import { PostAdService } from '../../postad.service';

@Component({
  selector: 'app-postad-create-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.less'],
  providers: [PostAdService]
})
export class CreateAdSelectCategoryComponent implements OnInit {

  ad$: Observable<Iad>;
  categories$: Observable<ICategory[]>;

  selectedBrowsCategory: ICategory;
  markForSelectionPath: ICategory[] = [];
  markForSelection: ICategory;

  @Output('onCategorySelected') onCategorySelected: EventEmitter<ICategory> = new EventEmitter<ICategory>();
  @Input() visibility: boolean;

  constructor(private store: Store<IAppState>, private postAdService: PostAdService) {}

  ngOnInit() {
    this.ad$ = this.store.select('postad');
    this.categories$ = this.store.select('layout').pluck('categories');
  }

  browsCategorySelect(category: ICategory) {
    this.selectedBrowsCategory = category;

    delete this.markForSelection;
    this.markForSelectionPath = [];
    this.markForSelectionPath.push(category);

    this.store.dispatch({
      type: GET_SUBCATEGORIES_BY_PARENT_RESET
    });

    this.store.dispatch({
      type: GET_SUBCATEGORIES_BY_PARENT,
      payload: {
        categoryID: category.id,
        index: 0
      }
    });
  }

  nextOrCheck(category: ICategory, index: number) {
    this.markForSelectionPath = this.postAdService.computePathForSelection<ICategory>(category, index, this.markForSelectionPath);

    if (category.hasChildrens) {

      this.store.dispatch({
        type: GET_SUBCATEGORIES_BY_PARENT,
        payload: { categoryID: category.id, index }
      });
    } else {

      this.store.dispatch({
        type: SET_CATEGORY_FOR_SELECTION,
        payload: {
          categoryID: category.id,
          index: index
        }
      });

      this.markForSelection = Object.assign({}, category, {
        path: this.markForSelectionPath
      });
    }
  }
}
