import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { LoginComponent } from './login/login.component';

const routes: Route[] = [
  { path: '', pathMatch: 'full', redirectTo: 'home/r'},
  { path: 'login', component: LoginComponent },
  { loadChildren: 'app/home/home.module#HomeModule', path: 'home' },
  { loadChildren: 'app/postad/postad.module#PostAdModule', path: 'postad' },
  { loadChildren: 'app/profile/profile.module#ProfileModule', path: 'profile' },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(
  routes,
  {
    useHash: false
  }
);
