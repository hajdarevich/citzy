/* tslint:disable:no-unused-variable */

import { TestBed, ComponentFixture, tick, fakeAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FooterComponent } from './footer.component';
import { By } from '@angular/platform-browser';

let component: FooterComponent;
let fixture: ComponentFixture<FooterComponent>;

describe('Footer component', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [FooterComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the footer component', () => {
    expect(component).toBeTruthy();
  });
});
