import { Component, ChangeDetectionStrategy, ViewChild, ElementRef, HostListener } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-ui-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})
export class FooterComponent {

  aboutUsActive: boolean = false;
  helpAndContactActive: boolean = false;
  moreFromUsActive: boolean = false;
  languageDropDownOpen: boolean;

  @ViewChild('footer') footer: ElementRef;
  @ViewChild('aboutUsEl') aboutUsEl: ElementRef;
  @ViewChild('helpAndContactEl') helpAndContactEl: ElementRef;
  @ViewChild('moreFromUsEl') moreFromUsEl: ElementRef;
  @ViewChild('languageDropDown') languageDropDown: ElementRef;

  @HostListener('click', ['$event.target'])
  clickOnFooterMobile($event) {
    // if it is clicked on About Us section
    if ($event.parentNode === this.aboutUsEl.nativeElement || $event.parentNode.parentNode === this.aboutUsEl.nativeElement) {
      this.aboutUsActive = !this.aboutUsActive;
    }

    // if it is clicked on Help and Contact us section
    if ($event.parentNode === this.helpAndContactEl.nativeElement || $event.parentNode.parentNode === this.helpAndContactEl.nativeElement) {
      this.helpAndContactActive = !this.helpAndContactActive;
    }

    // if it is clicked on More From Us us section
    if ($event.parentNode === this.moreFromUsEl.nativeElement || $event.parentNode.parentNode === this.moreFromUsEl.nativeElement) {
      this.moreFromUsActive = !this.moreFromUsActive;
    }
  }

  @HostListener('document:click', ['$event.target'])
  clickOnDocument($event) {
        // hide language dropdown
        if (this.languageDropDownOpen && $event !== this.languageDropDown.nativeElement
      && $event.parentNode !== this.languageDropDown.nativeElement) {
        this.languageDropDownOpen = false;
      }
  }


  /**
   * Opens language dropdown
   */
  openLanguageDropdown() {
    this.languageDropDownOpen = true;
  }
}
