import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.Default,
  selector: 'app-ui-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.less']
})
export class ModalComponent {
  @Input() visibility: boolean;
  @Input() title: string;
  @Input() height: number;

  closeModal() {
    this.visibility = false;
  }

  showModal() {
    this.visibility = true;
  }
}

