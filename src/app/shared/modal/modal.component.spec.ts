/* tslint:disable:no-unused-variable */

import { TestBed, ComponentFixture, tick, fakeAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ModalComponent } from './modal.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

let component:  ModalComponent;
let fixture:    ComponentFixture<ModalComponent>;

describe('Modal component: Tmp', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:        [ReactiveFormsModule, FormsModule],
      declarations:   [ModalComponent],
      providers:      [],

      schemas:        [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture     = TestBed.createComponent(ModalComponent);
    component   = fixture.debugElement.componentInstance;
  });

  it('should create the header component', () => {
    expect(component).toBeTruthy();
  });
});

