/* tslint:disable:no-unused-variable */

import { TestBed, ComponentFixture, async, tick, fakeAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SearchComponent } from './search.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { CATEGORIES } from '../megamenu/megamenu.component.spec';

let component: SearchComponent;
let fixture: ComponentFixture<SearchComponent>;

const event = {
  type: 'click',
  stopPropagation: function () {},
  preventDefault: function () {}
};

describe('Search component', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      declarations: [SearchComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the search component', () => {
    expect(component).toBeTruthy();
  });

  it('should have pre-defined value in input filed', fakeAsync(() => {
    component.search = 'Search value';
    fixture.detectChanges();
    tick();

    const el = fixture.debugElement.query(By.css('input[name=\'search\']'));

    expect(el.nativeElement.value).toBe('Search value');
  }));

  it('should open drop down menu', () => {
    component.open();
    fixture.detectChanges();

    const el = fixture.debugElement.query(By.css('div.menu'));

    expect(el.nativeElement.classList.contains('visible')).toBeTruthy();
    expect(component.dropdownOpen).toBeTruthy();
  });

  it('should emit searched values', fakeAsync(() => {
    component.search = 'car';
    component.location = 'Sarajevo';
    component.selected = 'All';

    fixture.detectChanges();
    tick();

    let form = {};
    component.onSearch.subscribe((emitted) => form = emitted);

    const el = fixture.debugElement.query(By.css('form.ui.form'));
    el.triggerEventHandler('submit', event);

    expect(form).toBeDefined();
    expect(form['search']).toBe('car');
    expect(form['location']).toBe('Sarajevo');
    expect(form['category']).toBe('All');
  }));

  it('should display categories', fakeAsync(() => {
    component.categories = [
      { id: 0, name: 'car', hasChildren: true, displayName: 'Car' },
      { id: 1, name: 'sale', hasChildren: true,  displayName: 'Sale' }
    ];
    fixture.detectChanges();
    tick();

    const el = fixture.debugElement.query(By.css('div.menu'));

    expect(el.children.length).toBe(2);
  }));

  it('should set correct category', fakeAsync(() => {
    component.selected = 'All';
    const before = component.selected;

    component.selected = 'Car';
    fixture.detectChanges();
    tick();

    expect(before).toBe('All');
    expect(component.selected).toBe('Car');
  }));

});
