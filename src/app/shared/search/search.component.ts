import {
  Component,
  Input,
  Output,
  ChangeDetectionStrategy,
  EventEmitter,
  ViewChild,
  ElementRef,
  HostListener
} from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';

import { ICategory, ISearchBox } from '../../store/layout/layout.reducer';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-ui-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent {
  @Input() categories: ICategory[];
  @Input() selected: string;
  @Input() search: string;
  @Input() location: string;

  @Output('onSearch') onSearch: EventEmitter<ISearchBox> = new EventEmitter<ISearchBox>();

  @ViewChild('dropDownEl') dropDownEl: ElementRef;

  dropdownOpen: boolean;

  onFormSubmited(event: Event, form: NgForm) {
    event.preventDefault();

    this.onSearch.next(Object.assign({}, form.value, { category: this.selected }));
  }

  @HostListener('keyup.esc')
  closeFromOutsideEsc() {
    if (this.dropdownOpen) {
      this.dropdownOpen = false;
    }
  }

  selectCategory(category: string) {
    this.selected = category;
  }

  @HostListener('document:click', ['$event.target'])
  closeFromOutsideClick($event) {
    const source = this.dropDownEl.nativeElement;

    // hide dropdown if it is open and if it is not clicked on it
    if (this.dropdownOpen && $event !== source && $event.parentNode !== source) {
      this.dropdownOpen = false;
    }
  }

  open() {
    this.dropdownOpen = true;
  }

  clearBox(fc: FormControl) {
    fc.reset();
  }
}
