import { NgModule } from '@angular/core';
import { ArraySplitIntoChunks } from './array/array.pipes';


@NgModule({
  declarations: [
    ArraySplitIntoChunks,
  ],
  exports: [
    ArraySplitIntoChunks
  ],
})
export class ArrayPipesModule {
}
