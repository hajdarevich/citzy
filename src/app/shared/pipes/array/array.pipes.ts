import { Pipe, PipeTransform } from '@angular/core';

type pipeObject = Object | string | number;

@Pipe({
  name: 'beArraySplitIntoChunks'
})
export class ArraySplitIntoChunks implements PipeTransform {

  transform(array: Array<pipeObject>, chanks: number): Array<pipeObject> {
    array = array || [];

    const groupSize: number = Math.ceil(array.length / chanks);

    return array
            .map((item, index) => index % groupSize === 0 ? array.slice(index, index + groupSize) : null)
            .filter((item) => item);
  }
}




