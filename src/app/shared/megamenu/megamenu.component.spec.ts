/* tslint:disable:no-unused-variable */

import { TestBed, ComponentFixture, tick, fakeAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MegaMenuComponent } from './megamenu.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

let component:  MegaMenuComponent;
let fixture:    ComponentFixture<MegaMenuComponent>;

export const CATEGORIES = [
  { id: 0, name: 'car', hasChildren: true, displayName: 'MOTORS', marketingText: 'Vehicles and Parts' },
  { id: 1, name: 'sale', hasChildren: true,  displayName: 'FOR SALE' }
];

describe('Megamenu component: Tmp', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:        [ReactiveFormsModule, FormsModule],
      declarations:   [MegaMenuComponent],
      providers:      [],

      schemas:        [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture     = TestBed.createComponent(MegaMenuComponent);
    component   = fixture.debugElement.componentInstance;
  });

  it('should create the header component', () => {
    expect(component).toBeTruthy();
  });

  it('should not display menu items if nothing is passed', fakeAsync(() => {

    component.menuItems = [];
    fixture.detectChanges();
    tick();

    const menuHeader = fixture.debugElement.query(By.css('.column.computer.only.header'));
    const menuMetaText = fixture.debugElement.query(By.css('.column.computer.only.header'));
    expect(menuHeader).toBeNull();
    expect(menuMetaText).toBeNull();

  }));

  it('should display menu items on big resolution ( computers only )', fakeAsync(() => {

    component.menuItems = CATEGORIES;
    component.visibility = true;
    fixture.detectChanges();
    tick();

    const menuHeader = fixture.debugElement.query(By.css('.column.computer.only .content .header'));
    expect(menuHeader.nativeElement.innerHTML).toBe('MOTORS');

  }));

  it('should display metadata on menu items on big resolution ( computers only )', fakeAsync(() => {

    component.menuItems = CATEGORIES;
    component.visibility = true;
    fixture.detectChanges();
    tick();

    const menuMetaItem = fixture.debugElement.query(By.css('.column.computer.only .content .meta span'));
    expect(menuMetaItem.nativeElement.innerHTML).toBe('Vehicles and Parts');

  }));

  it('should display menu items on medium resolution ( tablets only )', fakeAsync(() => {

    component.menuItems = CATEGORIES;
    component.visibility = true;
    fixture.detectChanges();
    tick();

    const menuHeader = fixture.debugElement.query(By.css('.column.tablet.only .content .header'));
    expect(menuHeader.nativeElement.innerHTML).toBe('MOTORS');

  }));

  it('should display menu items on small resolution ( mobile only )', fakeAsync(() => {

    component.menuItems = CATEGORIES;
    component.visibility = true;
    fixture.detectChanges();
    tick();

    const menuHeader = fixture.debugElement.query(By.css('.mobile.only .content .header'));
    expect(menuHeader.nativeElement.innerHTML).toBe('MOTORS');

  }));
});

