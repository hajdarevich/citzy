import {
  Component, ChangeDetectionStrategy, Input, Output, EventEmitter
} from '@angular/core';

import { ICategory } from '../../store/layout/layout.reducer';
import { ArraySplitIntoChunks } from '../pipes/array/array.pipes';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-ui-megamenu',
  templateUrl: './megamenu.component.html',
  styleUrls: ['./megamenu.component.less']
})
export class MegaMenuComponent {
  @Input() menuItems: ICategory[];
  @Input() isDimmerActive: boolean;
  @Input() isSubcategoryVisible: boolean;
  @Input() visibility: boolean;
  @Output('onMenuClicked') onMenuClicked: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output('backToMainMenuClicked') backToMainMenuClicked: EventEmitter<boolean> = new EventEmitter<boolean>();
  /* Local state */
  selectedCategoryId: number;
  selectedCategory: string;

  menuClick(id: number, name: string, showDimmer: boolean) {
    this.selectedCategoryId = id;
    this.selectedCategory = name;
    this.onMenuClicked.emit(showDimmer);
  }

  backToMainMenu(value: boolean) {
    this.backToMainMenuClicked.emit(value);
  }

  /**
   * Load sub categories and divide them into columns.
   *
   * @returns {Array<pipeObject>}
   */
  loadSubcategories(numberOfColumns: number) {

    const [category] = this.menuItems
      .filter((item: ICategory) => item.id === this.selectedCategoryId);

    if (numberOfColumns > 1) {
      return new ArraySplitIntoChunks().transform(category['subcategories'], numberOfColumns);
    } else {
      return category['subcategories'];
    }
  }
}

