import {Component, Input } from '@angular/core';

@Component({
  selector: 'app-accordion-group',
  templateUrl: './accordion.component.html',
})
export class AccordionGroupComponent {
  private _isOpen: boolean = false;

  @Input() title: string;
  @Input() className: string;

  @Input()
  set isOpen(value: boolean) {
    this._isOpen = value;
  }

  get isOpen() {
    return this._isOpen;
  }

  toggleOpen(event: MouseEvent): void {
    event.preventDefault();
    this.isOpen = !this.isOpen;
  }
}
