import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SearchComponent } from './search/search.component';

import { HeaderComponent } from './header/header.component';
import { MegaMenuComponent } from './megamenu/megamenu.component';
import { BannerComponent } from './banner/banner.component';
import { DimmerComponent } from './dimmer/dimmer.component';
import { ArrayPipesModule } from './pipes/array-pipes.module';
import { FooterComponent } from './footer/footer.component';
import { TopComponent } from './top/top.component';
import { ModalComponent } from './modal/modal.component';
import { AccordionGroupComponent } from './accordion/accordion.component';

@NgModule({
  declarations: [
    SearchComponent,
    DimmerComponent,
    HeaderComponent,
    FooterComponent,
    ModalComponent,
    MegaMenuComponent,
    BannerComponent,
    TopComponent,
    AccordionGroupComponent
  ],
  imports: [
    CommonModule,
    HttpModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ArrayPipesModule
  ],
  exports: [
    SearchComponent,
    DimmerComponent,
    HeaderComponent,
    FooterComponent,
    ModalComponent,
    MegaMenuComponent,
    BannerComponent,
    ArrayPipesModule,
    TopComponent,
    AccordionGroupComponent
  ]
})
export class SharedModule { }
