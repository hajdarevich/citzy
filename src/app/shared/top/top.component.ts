import { Component, ChangeDetectionStrategy, Input, HostListener, ViewChild, ElementRef, OnInit } from '@angular/core';
import { ArraySplitIntoChunks } from '../pipes/array/array.pipes';
import { ITopSearches, ITopLocations } from '../../store/layout/layout.reducer';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-ui-top-searches-location',
    templateUrl: 'top.component.html',
    styleUrls: ['top.component.less']
})
export class TopComponent {

  isGroupOpen: boolean = false;

  @Input() topsearches: ITopSearches[];
  @Input() toplocations: ITopLocations[];

  @ViewChild('topsearchesEl') topsearchesEl: ElementRef;
  @ViewChild('toplocationEl') toplocationEl: ElementRef;
  @ViewChild('topsearchesMobileEl') topsearchesMobileEl: ElementRef;
  @ViewChild('toplocationMobileEl') toplocationMobileEl: ElementRef;

  topSearchesActive = true;
  topSearchesMobileActive: boolean;
  topLocationMobileActive: boolean;

  @HostListener('click', ['$event.target'])
  topSearchesAndLocationClick($event) {
    // if it is clicked on topsearches set topsearches as active and toplocation as inactive segment
    if ($event === this.topsearchesEl.nativeElement ) {
      this.topSearchesActive = true;
      this.topSearchesMobileActive = true;
    }

    // if it is clicked on toplocation set toplocation as active and topsearches as inactive segment
    if ($event === this.toplocationEl.nativeElement ) {
      this.topSearchesActive = false;
      this.topLocationMobileActive = false;
    }

    // if it is clicked Top Searches on mobile resolution
    if ($event.parentNode === this.topsearchesMobileEl.nativeElement ||
      $event.parentNode.parentNode === this.topsearchesMobileEl.nativeElement) {
      this.topSearchesMobileActive = !this.topSearchesMobileActive;
    }

    // if it is clicked on Top Location on mobile resolution
    if ($event.parentNode === this.toplocationMobileEl.nativeElement ||
      $event.parentNode.parentNode === this.toplocationMobileEl.nativeElement) {

      this.topLocationMobileActive = !this.topLocationMobileActive;
    }
  }

  /**
   * Load top searches and top location.
   *
   * @returns {Array<pipeObject>}
   */
  loadTopSearches(numberOfColumns: number) {
    const topsearches = this.topsearches;
    return new ArraySplitIntoChunks().transform(topsearches, numberOfColumns);
  }

}

export const TOP_SEARCHES: ITopSearches[] = [
  {
    name: 'Android',
    link: '#'
  },
  {
    name: 'IPhone',
    link: '#'
  },
  {
    name: 'Angular JS',
    link: '#'
  },
  {
    name: 'Knockout JS',
    link: '#'
  },
  {
    name: 'CodeIgniter',
    link: '#'
  },
  {
    name: 'Android Developers',
    link: '#'
  },
  {
    name: 'AngularJS Developers',
    link: '#'
  },
  {
    name: 'Bookkepers',
    link: '#'
  },
  {
    name: 'C# Developers',
    link: '#'
  },
  {
    name: 'Objective C Developers',
    link: '#'
  },
  {
    name: 'UI Designer',
    link: '#'
  },
  {
    name: 'UX Design',
    link: '#'
  },
  {
    name: 'Wordpress Developers',
    link: '#'
  },
  {
    name: 'NodeJS Experts',
    link: '#'
  },
  {
    name: 'Innovic',
    link: '#'
  },
  {
    name: 'Citzy',
    link: '#'
  },
  {
    name: 'Semantic UI',
    link: '#'
  },
  {
    name: 'Laravel',
    link: '#'
  },
  {
    name: 'Mongoose',
    link: '#'
  },
  {
    name: 'Mongo DB',
    link: '#'
  }
];

export const TOP_LOCATIONS: ITopLocations[] = [
  {
    title: 'Top cities',
    cities: [
      {
        name: 'Belfast',
        link: '#'
      },
      {
        name: 'Birmingham',
        link: '#'
      },
      {
        name: 'Brighton',
        link: '#'
      },
      {
        name: 'Bristol',
        link: '#'
      },
      {
        name: 'Cardiff',
        link: '#'
      },
      {
        name: 'Edinburg',
        link: '#'
      },
      {
        name: 'Glasgow',
        link: '#'
      },
      {
        name: 'Leeds',
        link: '#'
      },
      {
        name: 'Liverpool',
        link: '#'
      },
      {
        name: 'London',
        link: '#'
      },
      {
        name: 'Manchester',
        link: '#'
      },
      {
        name: 'Sheffield',
        link: '#'
      }
    ]
  },
  {
    title: 'England',
    cities: [
      {
        name: 'Manchester',
        link: '#'
      },
      {
        name: 'Sheffield',
        link: '#'
      },
      {
        name: 'Cardiff',
        link: '#'
      },
      {
        name: 'Edinburg',
        link: '#'
      },
      {
        name: 'Glasgow',
        link: '#'
      }
    ]
  },
  {
    title: 'Europe',
    cities: [
      {
        name: 'Sarajevo',
        link: '#'
      },
      {
        name: 'Berlin',
        link: '#'
      },
      {
        name: 'Wien',
        link: '#'
      },
      {
        name: 'Rome',
        link: '#'
      },
      {
        name: 'Madrid',
        link: '#'
      }
    ]
  },
];
