import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

interface Image {
  imageId: string;
  size?: string;
  image?: string;
}

interface IHomeBanner {
  marketingText?: string;
  images?: Image[];
}

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-ui-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.less']
})
export class BannerComponent {
  @Input() banner: IHomeBanner;

  visibilityClass(index: number): string {

    switch (index) {
      case 1:
        return 'tablet';
      case 2:
        return 'mobile';
      default:
          return 'computer';
    }
  }
}
