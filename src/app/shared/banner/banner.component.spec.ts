/* tslint:disable:no-unused-variable */

import { TestBed, ComponentFixture, tick, fakeAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BannerComponent } from './banner.component';
import { By } from '@angular/platform-browser';
import { Observable } from 'rxjs';

let component: BannerComponent;
let fixture: ComponentFixture<BannerComponent>;

describe('Banner component', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [BannerComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(BannerComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the banner component', () => {
    expect(component).toBeTruthy();
  });
  /*
  it('should not display image if nothing is passed', fakeAsync(() => {
    component.banner = Observable.of({ images: [{image: 'data:image/jpg;base64,R0lGODlhPQ'}]});
    fixture.detectChanges();
    tick();

    const el = fixture.debugElement.query(By.css('img'));

    expect(el).toBeNull();
  }));

  it('should display image if we pass image location', fakeAsync(() => {
    component.banner = Observable.of({ images: [{image: 'data:image/jpg;base64,R0lGODlhPQ'}]});
    fixture.detectChanges();
    tick();

    const el = fixture.debugElement.query(By.css('img'));

    expect(el.nativeElement).toBeDefined();
  }));
  */
});
