import {
    Component, ChangeDetectionStrategy, Input, HostListener, ViewChild,
    ElementRef, Output, EventEmitter
} from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-ui-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent {
  @Input() title: string;
  @Input() mobileMenuActive: boolean;
  @Output('onMobileMenuClicked') onMobileMenuClicked: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output('onMobileMenuItemClicked') onMobileMenuItemClicked: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('dropDownEl') dropDownEl: ElementRef;
  @ViewChild('dropDownElMobileMenu') dropDownElMobileMenu: ElementRef;

  dropdownOpen: boolean;
  dropDownMobileOpen: boolean;

  @HostListener('keyup.esc')
  closeFromOutsideEsc() {
    if (this.dropdownOpen) {
      this.dropdownOpen = false;
    }
  }

  @HostListener('document:click', ['$event.target'])
  closeFromOutsideClick($event) {
    const source       = this.dropDownEl.nativeElement;
    const mobileSource = this.dropDownElMobileMenu.nativeElement;

    // hide my citzy dropdown if it is open and if it is not clicked on it
    if (this.dropdownOpen && $event !== source && $event.parentNode !== source) {
      this.dropdownOpen = false;
    }

    // hide mobile menu dropdown if it is open and if it is not clicked on it
    if (this.dropDownElMobileMenu && $event !== mobileSource && $event.parentNode !== mobileSource && !$event.hasAttribute('disabled')) {
      this.dropDownMobileOpen = false;
    }
  }

  /**
   * Opens dropdown for My Citzy
   */
  open() {
    this.dropdownOpen = true;
  }

  navigate(url: string) {
    this.dropDownMobileOpen = false;
    this.onMobileMenuItemClicked.emit(url);
  }

  /**
   * Opens mobile menu
   */
  openMobileMenu($event) {
    if ($event === this.dropDownElMobileMenu.nativeElement || $event.parentNode === this.dropDownElMobileMenu.nativeElement) {
      this.dropDownMobileOpen = true;
      this.onMobileMenuClicked.emit(true);
    }
  }
}
