/* tslint:disable:no-unused-variable */

import { TestBed, ComponentFixture, async, tick, fakeAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HeaderComponent } from './header.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

let component:  HeaderComponent;
let fixture:    ComponentFixture<HeaderComponent>;

describe('Header component: Tmp', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:        [ReactiveFormsModule, FormsModule],
      declarations:   [HeaderComponent],
      providers:      [],

      schemas:        [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture     = TestBed.createComponent(HeaderComponent);
    component   = fixture.debugElement.componentInstance;
  });

  it('should create the header component', () => {
    expect(component).toBeTruthy();
  });


  it('should have pre-defined title in the input field for logo and title', fakeAsync(() => {
    component.title = 'Citzy.com';
    fixture.detectChanges();
    tick();

    const el = fixture.debugElement.query(By.css('.header-text'));
    expect(el.nativeElement.title).toBe('Citzy.com');

  }));

  it('should open Citzy drop down menu', () => {
    component.open();
    fixture.detectChanges();

    const el = fixture.debugElement.query(By.css('div.my_citzy_dropdown'));

    expect(el.nativeElement.classList.contains('visible')).toBeTruthy();
    expect(component.dropdownOpen).toBeTruthy();
  });

  it('should open mobile menu', () => {
    component.openMobileMenu();
    fixture.detectChanges();

    const el = fixture.debugElement.query(By.css('div.mobile_menu'));

    expect(el.nativeElement.classList.contains('visible')).toBeTruthy();
    expect(component.dropDownMobileOpen).toBeTruthy();
  });

  // @TODO make test for closing dropdowns
});
