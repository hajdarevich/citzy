import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-ui-dimmer',
  templateUrl: './dimmer.component.html',
  styleUrls: ['./dimmer.component.less']
})
export class DimmerComponent {
  @Input() visibility: boolean;
  @Output('onDimmerClose') onDimmerClose: EventEmitter<boolean> = new EventEmitter<boolean>();
}
