/* tslint:disable:no-unused-variable */

import { TestBed, ComponentFixture, tick, fakeAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { DimmerComponent } from './dimmer.component';
import { By } from '@angular/platform-browser';

let component: DimmerComponent;
let fixture: ComponentFixture<DimmerComponent>;

describe('Dimmer component', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [DimmerComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(DimmerComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the banner component', () => {
    expect(component).toBeTruthy();
  });
  /*
  it('should not display dimmer', fakeAsync(() => {
    component.visibility = false;
    fixture.detectChanges();
    tick();

    const el = fixture.debugElement.query(By.css('div.ui.dimmer'));

    expect(el).toBeNull();
  }));

  it('should display dimmer', fakeAsync(() => {
    component.visibility = true;
    fixture.detectChanges();
    tick();

    const el = fixture.debugElement.query(By.css('div.ui.dimmer'));

    expect(el.nativeElement).toBeDefined();
  }));
  */
});
