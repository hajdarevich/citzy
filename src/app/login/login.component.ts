import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { IAppState } from '../store/index';
import { CREATE_AD } from '../store/postad/postad.actions';
import { IProfile } from '../store/profile/profile.reducer';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  postAd$: Observable<IProfile>;

  constructor(private store: Store<IAppState>) {}

  ngOnInit() {
    this.postAd$ = this.store.select('postad');
  }

  createAdGuest(guest: boolean) {

    this.store.dispatch({
      type: CREATE_AD,
      payload: {
        newUser: guest
      }
    });
  }
}
