import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { IAppState } from './store/index';
import { ILayout } from './store/layout/layout.reducer';
import {
    SHOW_DIMMER, HIDE_DIMMER, SUBMIT_SEARCH, GET_SUBCATEGORIES, APP_START,
    HIDE_SUBCATEGORIES, SHOW_SUBCATEGORIES
} from './store/layout/layout.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {

  layout$: Observable<ILayout>;
  /**
   * mobileMenuActive
   * Used for handling z-index on megamenu and header component.
   * We need to have this in case when mobile menu is active in that case
   * we want dimmer over everything expect mobile menu. Without this megamenu will not be
   * dimmed.
   * @type {boolean}
   */
  mobileMenuActive = false;

  constructor(private store: Store<IAppState>, private router: Router) {}

  ngOnInit() {
    this.layout$ = this.store.select('layout');

    this.store.dispatch({
      type: APP_START
    });
  }

  onSearchBox(searchObject: {}) {
    this.store.dispatch({
      type: SUBMIT_SEARCH,
      payload: searchObject
    });
  }

  showDimmer() {
    this.store.dispatch({
      type: SHOW_DIMMER
    });
  }

  mobileMenuDimmer() {
    this.mobileMenuActive = true;

    this.showDimmer();
  }

  goToLocation(url: string) {
    this.hideDimmer();

    this.router.navigateByUrl(url);
  }

  hideDimmer() {
    this.mobileMenuActive = false;
    this.hideSubcategories();

    this.store.dispatch({
      type: HIDE_DIMMER
    });

  }

  showSubcategories(showDimmer: boolean) {
    if (showDimmer) {
      this.showDimmer();
    }

    this.store.dispatch({
      type: SHOW_SUBCATEGORIES
    });
  }

  hideSubcategories() {
    this.store.dispatch({
      type: HIDE_SUBCATEGORIES
    });
  }

}
