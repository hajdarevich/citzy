import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { routing } from './app.router';
import { effects, store, i } from './store';
import { SharedModule } from './shared/shared.module';
import { LoginComponent } from './login/login.component';
import { ArrayPipesModule } from './shared/pipes/array-pipes.module';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    FormsModule,
    HttpModule,
    ArrayPipesModule,
    store,
    effects,
    routing,
    i
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {}
