import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-ui-home-myalerts',
    templateUrl: 'myalerts.component.html'
})
export class MyalertsComponent {

}
