import {
  Component, ChangeDetectionStrategy, AfterViewInit, ViewChild, ElementRef, ViewChildren,
  QueryList
} from '@angular/core';
import { Store} from '@ngrx/store';
import { Observable } from 'rxjs';

import { IAppState } from '../../../store/index';

declare var Masonry: any;

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-ui-home-recommended',
    templateUrl: 'recommended.component.html',
    styleUrls: ['recommended.component.less']
})
export class RecommendedComponent implements AfterViewInit {

  recommendedCards$: Observable<{}>;

  @ViewChild('masonryGrid') masonryGrid: ElementRef;
  @ViewChildren('items') items: QueryList<ElementRef>;

  constructor(private store: Store<IAppState>) {
    this.recommendedCards$ = this.store
      .select('cards')
      .pluck('recommended');
  }

  ngAfterViewInit(): void {

    this.items
      .changes
      .debounceTime(500)
      .subscribe((data) => {
        if (data) {
          new Masonry(this.masonryGrid.nativeElement, {});
        }
      });

    this.items.notifyOnChanges();
  }
}

