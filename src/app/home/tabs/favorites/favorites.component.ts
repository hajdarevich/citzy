import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-ui-home-favorites',
    templateUrl: 'favorites.component.html'
})
export class FavoritesComponent {

}
