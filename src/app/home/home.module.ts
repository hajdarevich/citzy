import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeComponent } from './home.component';
import { routing } from './home.router';
import { SharedModule } from '../shared/shared.module';
import { FavoritesComponent } from './tabs/favorites/favorites.component';
import { RecommendedComponent } from './tabs/recommended/recommended.component';
import { MyalertsComponent } from './tabs/myalerts/myalerts.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ],
  declarations: [
    HomeComponent,
    FavoritesComponent,
    RecommendedComponent,
    MyalertsComponent
  ],
  bootstrap: [
    HomeComponent
  ]
})
export class HomeModule {}
