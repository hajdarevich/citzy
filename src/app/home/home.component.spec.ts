/* tslint:disable:no-unused-variable */

import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpModule } from '@angular/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HomeComponent } from './home.component';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '../shared/shared.module';

let component:  HomeComponent;
let fixture:    ComponentFixture<HomeComponent>;

describe('Home component: Tmp', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:        [HttpModule, RouterTestingModule, StoreModule.provideStore({}), SharedModule],
      declarations:   [HomeComponent],
      providers:      [],

      schemas:        [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture     = TestBed.createComponent(HomeComponent);
    component   = fixture.debugElement.componentInstance;
  });

  it('should create the home component', () => {
    expect(component).toBeTruthy();
  });

});
