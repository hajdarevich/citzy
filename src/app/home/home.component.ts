import { Component, OnInit, OnDestroy } from '@angular/core';
import { IAppState } from '../store/index';
import { Store } from '@ngrx/store';

import {
  SHOW_BANNER, HIDE_BANNER, SHOW_MENU, HIDE_MENU,
  SHOW_TOPSEARCHESLOCATION, HIDE_TOPSEARCHESLOCATION
} from '../store/layout/layout.actions';

@Component({
  selector: 'app-ui-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit, OnDestroy {

  constructor(private store: Store<IAppState>) {}

  ngOnInit() {

    this.store.dispatch({
      type: SHOW_BANNER
    });

    this.store.dispatch({
      type: SHOW_MENU
    });

    this.store.dispatch({
      type: SHOW_TOPSEARCHESLOCATION
    });
  }

  ngOnDestroy() {

    this.store.dispatch({
      type: HIDE_BANNER
    });

    this.store.dispatch({
      type: HIDE_MENU
    });

    this.store.dispatch({
      type: HIDE_TOPSEARCHESLOCATION
    });
  }
}
