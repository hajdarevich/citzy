import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { HomeComponent } from './home.component';
import { FavoritesComponent } from './tabs/favorites/favorites.component';
import { RecommendedComponent } from './tabs/recommended/recommended.component';
import { MyalertsComponent } from './tabs/myalerts/myalerts.component';

const routes: Route[] = [
  {
    path: '',
    component: HomeComponent,
    children: [{
      path: 'f',
      component: FavoritesComponent
    },
    {
      path: 'r',
      component: RecommendedComponent
    },
    {
      path: 'a',
      component: MyalertsComponent
    },
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
