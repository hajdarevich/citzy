export const APP_START = 'Layout: application start';

export const SHOW_BANNER = 'Layout: show banner';
export const HIDE_BANNER = 'Layout: hide banner';

export const SHOW_MENU = 'Layout: show mega menu';
export const HIDE_MENU = 'Layout: hide mega menu';

export const SHOW_DIMMER = 'Layout: show dimmer';
export const HIDE_DIMMER = 'Layout: hide dimmer';

export const GET_CATEGORIES = 'Layout: load categories';
export const GET_CATEGORIES_SUCCESS = 'Layout: load categories success';
export const GET_CATEGORIES_FAIL = 'Layout: load categories fail';

export const GET_LOCATIONS_SUCCESS = 'Layout: load countries success';
export const GET_LOCATIONS_FAIL = 'Layout: load countries faild';

export const SUBMIT_SEARCH = 'Layout: search box submitted';

export const RESET_LAYOUT_STATE = 'Layout: reset state';

export const GET_SUBCATEGORIES = 'Layout: load sub-categories';
export const GET_SUBCATEGORIES_SUCCESS = 'Layout: load sub-categories success';
export const GET_SUBCATEGORIES_FAIL = 'Layout: load sub-categories fail';

export const GET_RANDOM_BANNER_SUCCESS = 'Layout: load random banner success';
export const GET_RANDOM_BANNER_FAIL = 'Layout: load random banner fail';

export const GET_RANDOM_BANNER_IMAGES_SUCCESS = 'Layout: load random banner images success';
export const GET_RANDOM_BANNER_IMAGES_FAIL = 'Layout: load random banner images fail';

export const SHOW_SUBCATEGORIES = 'Layout: show sub-categories';
export const HIDE_SUBCATEGORIES = 'Layout: hide sub-categories';

export const SHOW_TOPSEARCHESLOCATION = 'Layout: show top searches and location';
export const HIDE_TOPSEARCHESLOCATION = 'Layout: hide top searches and location';

export const GET_TOPSEARCHES = 'Layout: load topsearches';
export const GET_TOPSEARCHES_SUCCESS = 'Layout: load topsearches success';
export const GET_TOPSEARCHES_FAIL = 'Layout: load topsearches fail';

export const GET_TOPLOCATIONS = 'Layout: load toplocations';
export const GET_TOPLOCATIONS_SUCCESS = 'Layout: load toplocations success';
export const GET_TOPLOCATIONS_FAIL = 'Layout: load toplocations fail';

