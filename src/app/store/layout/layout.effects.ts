import { Effect, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';

import {
  GET_CATEGORIES_SUCCESS, GET_CATEGORIES_FAIL,
  GET_SUBCATEGORIES_SUCCESS, APP_START, GET_SUBCATEGORIES_FAIL, GET_LOCATIONS_SUCCESS, GET_LOCATIONS_FAIL,
  GET_RANDOM_BANNER_SUCCESS, GET_RANDOM_BANNER_FAIL, GET_TOPSEARCHES_SUCCESS, GET_TOPLOCATIONS_SUCCESS
} from './layout.actions';
import { ICategory, IHomeBanner } from './layout.reducer';
import { TOP_SEARCHES, TOP_LOCATIONS } from '../../shared/top/top.component';

@Injectable()
export class LayoutEffects {

  @Effect()
  getCategories$ = this.actions$
    .ofType(APP_START)
    .switchMap(() => {

      return this.http
        .get('http://api.citzy.com/api/services/app/Category/GetByParent')
        .map((response: Response) => response.json())
        .map((categories: {}) => ({ payload: categories, type: GET_CATEGORIES_SUCCESS }))
        .catch((error: Response) => Observable.of(({ payload: error, type: GET_CATEGORIES_FAIL })));
    });

  @Effect()
  getLocations$ = this.actions$
    .ofType(APP_START)
    .switchMap(() => {

      return this.http
        .get('http://api.citzy.com/api/services/app/Location/GetByParent')
        .map((response: Response) => response.json())
        .map((locations: {}) => ({ payload: locations, type: GET_LOCATIONS_SUCCESS }))
        .catch((error: Response) => Observable.of(({ payload: error, type: GET_LOCATIONS_FAIL })));
    });

  @Effect()
  getRandomBanner$ = this.actions$
    .ofType(APP_START)
    .switchMap(() => {

      return this.http
        .get('http://api.citzy.com/api/services/app/Banner/GetRandom')
        .map((response: Response) => response.json())
        .map((banners: IHomeBanner) => ({ payload: banners, type: GET_RANDOM_BANNER_SUCCESS }))
        .catch((error: Response) => Observable.of(({ payload: error, type: GET_RANDOM_BANNER_FAIL })));
    });

  @Effect()
  getMegaMenuSubCategories$ = this.actions$
    .ofType(GET_CATEGORIES_SUCCESS)
    .switchMap((action: Action) => {

      let subcategories = [];

      action.payload.result.items.map((category: ICategory) => {
        subcategories.push(
          this.http.get('http://api.citzy.com/api/services/app/Category/GetByParent?ParentId=' + category.id)
            .map((response: Response) => response.json())
            .map((response: { result: { items: Array<Object> } }) => {
              return Object.assign({}, category, {
                subcategories: response.result.items
              });
            })
        );
      });

      return Observable.combineLatest(subcategories)
        .map((category: Array<ICategory>) => {
          return { payload: category, type: GET_SUBCATEGORIES_SUCCESS };
        })
        .catch((error: Response) => Observable.of(({ payload: error, type: GET_SUBCATEGORIES_FAIL })));
    });

  @Effect()
  getTopSearches$ = this.actions$
    .ofType(APP_START)
    .map(() => {
      return ({ payload: TOP_SEARCHES, type: GET_TOPSEARCHES_SUCCESS });
    });

  @Effect()
  getTopLocations$ = this.actions$
    .ofType(APP_START)
    .map(() => {
      return ({ payload: TOP_LOCATIONS, type: GET_TOPLOCATIONS_SUCCESS });
    });

  constructor(private actions$: Actions, private http: Http) {}
}
