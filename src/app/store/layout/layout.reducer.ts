import { Action, ActionReducer } from '@ngrx/store';
import {
  HIDE_BANNER, SHOW_BANNER, HIDE_MENU, SHOW_MENU, RESET_LAYOUT_STATE, SHOW_DIMMER,
  HIDE_DIMMER, GET_CATEGORIES_SUCCESS, SUBMIT_SEARCH, GET_SUBCATEGORIES_SUCCESS, HIDE_SUBCATEGORIES,
  SHOW_SUBCATEGORIES, GET_LOCATIONS_SUCCESS, GET_RANDOM_BANNER_SUCCESS, GET_RANDOM_BANNER_IMAGES_SUCCESS,
  SHOW_TOPSEARCHESLOCATION, HIDE_TOPSEARCHESLOCATION, GET_TOPSEARCHES_SUCCESS, GET_TOPLOCATIONS_SUCCESS
} from './layout.actions';

export interface IAttributes {
  id: string;
  displayName: string;
  sellerType: {
    displayName: string;
    values: Array<{
      displayName: string;
      name: string;
    }>
  };
}

export interface ICategory {
  id: number;
  active?: boolean;
  displayName: string;
  icon?: string;
  name?: string;
  marketingText?: string;
  hasChildrens?: boolean;
  path?: ICategory[];
  subcategories?: ISubCategory[];
  /* Attributes */
  attributes?: IAttributes;
}

export interface IRecommendedCards {
  image: string;
  name: string;
  description: string;
  price: number;
}

export interface ILocation {
  id: number;
  displayName: string;
  code: string;
  hasChildrens: boolean;
}

export interface ISubCategory extends ICategory {
  marketingText?: string;
}

export interface ISearchBox {
  category?: string;
  search?: string;
  location?: string;
}

export interface Image {
  imageId: string;
  size?: string;
  image?: string;
}

export interface IHomeBanner {
  marketingText?: string;
  images?: Image[];
}

export interface ITopSearches {
  name: string;
  link?: string;
}

export interface  ITopLocations {
  title?: string;
  cities: Array<ITopLocation>;
}

export interface ITopLocation {
  name: string;
  link?: string;
}


export interface ILayout {
  isBannerVisible: boolean;
  isTopSearchesLocationVisible: boolean;
  isMegaMenuVisible: boolean;
  isDimmerVisible: boolean;
  isSubcategoriesVisible: boolean;
  areRecommendedCardsLoaded: boolean;
  searchBox: ISearchBox;
  randomBanner?: IHomeBanner;
  categories: ICategory[];
  locations: ILocation[];
  topsearches: ITopSearches[];
  toplocations: ITopLocations[];
}

const initialState: ILayout = {
  isBannerVisible: false,
  isTopSearchesLocationVisible: false,
  isMegaMenuVisible: false,
  isDimmerVisible: false,
  isSubcategoriesVisible: false,
  areRecommendedCardsLoaded: false,
  searchBox: {},
  categories: [],
  locations: [],
  topsearches: [],
  toplocations: []
};

export const layoutReducer: ActionReducer<ILayout> = (state: ILayout = initialState, action: Action): ILayout => {

  switch (action.type) {

    case HIDE_BANNER:

      return Object.assign({}, state, {
        isBannerVisible: false
      });

    case SHOW_BANNER:

      return Object.assign({}, state, {
        isBannerVisible: true
      });

    case HIDE_MENU:

      return Object.assign({}, state, {
        isMegaMenuVisible: false
      });

    case SHOW_MENU:

      return Object.assign({}, state, {
        isMegaMenuVisible: true
      });

    case SHOW_DIMMER:

      return Object.assign({}, state, {
        isDimmerVisible: true
      });

    case HIDE_DIMMER:

      return Object.assign({}, state, {
        isDimmerVisible: false
      });

    case SHOW_SUBCATEGORIES:

      return Object.assign({}, state, {
        isSubcategoriesVisible: true
      });

    case HIDE_SUBCATEGORIES:

      return Object.assign({}, state, {
        isSubcategoriesVisible: false
      });

    case GET_CATEGORIES_SUCCESS:

      return Object.assign({}, state, {
        categories: action.payload.result.items
      });

    case GET_LOCATIONS_SUCCESS:

      return Object.assign({}, state, {
        locations: action.payload.result.items
      });

    case GET_SUBCATEGORIES_SUCCESS:

      return Object.assign({}, state, {
        categories: action.payload
      });

    case GET_RANDOM_BANNER_SUCCESS:

      return Object.assign({}, state, {
        randomBanner: action.payload.result
      });

    case GET_TOPSEARCHES_SUCCESS:

      return Object.assign({}, state, {
        topsearches: action.payload
      });

    case GET_TOPLOCATIONS_SUCCESS:

      return Object.assign({}, state, {
        toplocations: action.payload
      });

    case GET_RANDOM_BANNER_IMAGES_SUCCESS:

      return Object.assign({}, state, {
        randomBanner: Object.assign({}, state.randomBanner, {
          images: action.payload
        })
      });

    case SUBMIT_SEARCH:

      return Object.assign({}, state, {
        searchBox: action.payload
      });

    case SHOW_TOPSEARCHESLOCATION:

      return Object.assign({}, state, {
        isTopSearchesLocationVisible: true
      });

    case HIDE_TOPSEARCHESLOCATION:

      return Object.assign({}, state, {
        isTopSearchesLocationVisible: false
      });

    case RESET_LAYOUT_STATE:

      return Object.assign({}, initialState);

    default:
      return state;
  }
};
