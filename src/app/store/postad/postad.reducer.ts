import { Action, ActionReducer } from '@ngrx/store';
import {
  CREATE_AD_SUCCESS, RESET_POSTAD_STATE, CREATE_AD,
  CREATE_AD_SEARCH_CATEGORY_SUCCESS, CREATE_AD_SEARCH_CATEGORY, CREATE_AD_SELECT_CATEGORY, CREATE_AD_CLEAR_CATEGORY,
  CREATE_AD_SEARCH_CATEGORY_FAIL, GET_SUBCATEGORIES_BY_PARENT_SUCCESS, GET_SUBCATEGORIES_BY_PARENT_RESET,
  SET_CATEGORY_FOR_SELECTION, CREATE_AD_SEARCH_LOCATION, CREATE_AD_SEARCH_LOCATION_SUCCESS, CREATE_AD_SELECT_LOCATION,
  CREATE_AD_CLEAR_LOCATION, CREATE_AD_SEARCH_LOCATION_CLEAR, GET_LOCATIONS_BY_PARENT_SUCCESS, REST_LOCATIONS,
  SET_LOCATION_FOR_SELECTION, SET_SELLER_TYPE, SET_TITLE, SET_DESCRIPTION, SET_PRICE, SET_INCLUDE_LINK,
  SET_WEBSITE, SET_PROMOTION, SET_YOUTUBE, LOAD_AD_SUCCESS, CREATE_AD_CATEGORY_LOAD_ATTRIBUTES_SUCCESS, SET_IMAGE,
  REMOVE_IMAGE
} from './postad.actions';
import { ICategory } from '../layout/layout.reducer';

export interface ILocation {
  id: number;
  active?: boolean;
  hasChildrens?: boolean;
  displayName: string;
  path?: string;
  type?: string;
}

export interface Iad {
  id?: string;
  category?: ICategory;
  location?: string;
  title?: string;
  description?: string;
  price?: number;
  sellerType?: string;
  newUser?: boolean;
  web?: string;
  images?: string [];
  youtube?: string;
  promotion?: Object;
  includeLink?: boolean;
  /* helper variables */
  adPrice?: number;
  promotionPrice?: number;
  suggestions?: ICategory[];
  locationSuggestions?: ILocation[];
  categoryLevels?: ICategory[][];
  locationsLevels?: ILocation[][];
  isFetchingSuggestions?: boolean;
  isFetchingLocation?: boolean;
  isCreatingAd: boolean;
}

const calculatePromotionPrice = (values: Object): number => {

  let total = 0;

  if (values['all'] && values['urgent'] && values['featured'] && values['spotlight']) {
    return Number(values['urgent_price']) + Number(values['featured_price']) + Number(values['spotlight_price']);
  }

  if (values['urgent']) {
    total += Number(values['urgent_price']);
  }

  if (values['featured']) {
    total += Number(values['featured_price']);
  }

  if (values['spotlight']) {
    total += Number(values['spotlight_price']);
  }

  return total;
};


export const initialState: Iad = {
  isCreatingAd: false,
  adPrice: 0,
  promotionPrice: 0
};

export const postadReducer: ActionReducer<Iad> = (state: Iad = initialState, action: Action): Iad => {

  switch (action.type) {

    case CREATE_AD:

      return Object.assign({}, {
        isCreatingAd: true
      });

    case CREATE_AD_SUCCESS:

      return Object.assign({}, initialState, action.payload, {
        isCreatingAd: false
      });

    case LOAD_AD_SUCCESS:

      return Object.assign({}, state, action.payload);

    case CREATE_AD_SEARCH_CATEGORY:

      return Object.assign({}, state, {
        isFetchingSuggestions: true,
        suggestions: []
      });

    case CREATE_AD_SEARCH_CATEGORY_SUCCESS:

      return Object.assign({}, state, {
        isFetchingSuggestions: false,
        suggestions: action.payload.result.items
      });

    case CREATE_AD_SEARCH_CATEGORY_FAIL:

      return Object.assign({}, state, {
        isFetchingSuggestions: false,
        suggestions: []
      });

    case CREATE_AD_SELECT_CATEGORY:

      return Object.assign({}, state, {
        category: action.payload,
        sellerType: null
      });

    case CREATE_AD_CLEAR_CATEGORY:

      return Object.assign({}, state, {
        category: null,
        suggestions: null
      });

    case GET_SUBCATEGORIES_BY_PARENT_SUCCESS:

      let currentCategoryLevels = state.categoryLevels || [];

      if (currentCategoryLevels[action.payload.index]) {
        currentCategoryLevels = [...currentCategoryLevels.slice(0, action.payload.index), action.payload.result.children];
      } else {
        currentCategoryLevels = [...currentCategoryLevels, action.payload.result.children];
      }

      if (currentCategoryLevels[action.payload.index - 1]) {
        currentCategoryLevels[action.payload.index - 1] = currentCategoryLevels[action.payload.index - 1]
          .map((category: ICategory) => {
            return Object.assign({}, category, {
              active: category.id === action.payload.categoryID
            });
          });
      }

      return Object.assign({}, state, {
        categoryLevels: currentCategoryLevels
      });

    case SET_CATEGORY_FOR_SELECTION:

      return Object.assign({}, state, {
        categoryLevels: state.categoryLevels
          .map((categories: ICategory[], index: number) => categories
            .map((item: ICategory) => {
              return Object.assign({}, item, {
                active: (!item.active) ? item.id === action.payload.categoryID : (item.active && !item.hasChildrens) ? false : item.active
              });
            }))
      });

    case GET_SUBCATEGORIES_BY_PARENT_RESET:

      return Object.assign({}, state, {
        categoryLevels: []
      });

    case CREATE_AD_CATEGORY_LOAD_ATTRIBUTES_SUCCESS:

      return Object.assign({}, state, {
        category: Object.assign({}, state.category, {
          attributes: action.payload
        })
      });

    case CREATE_AD_SEARCH_LOCATION:

      return Object.assign({}, state, {
        isFetchingLocation: true
      });

    case CREATE_AD_SEARCH_LOCATION_SUCCESS:

      return Object.assign({}, state, {
        locationSuggestions: action.payload.result.items,
        isFetchingLocation: false
      });

    case CREATE_AD_SEARCH_LOCATION_CLEAR:

      return Object.assign({}, state, {
        locationSuggestions: null,
        locationsLevels: []
      });

    case CREATE_AD_SELECT_LOCATION:

      return Object.assign({}, state, {
        location: action.payload
      });

    case CREATE_AD_CLEAR_LOCATION:

      return Object.assign({}, state, {
        location: null,
        locationSuggestions: null
      });

    case REST_LOCATIONS:

      return Object.assign({}, state, {
        locationsLevels: null
      });

    case GET_LOCATIONS_BY_PARENT_SUCCESS:

      let currentLocationsLevels = state.locationsLevels || [];

      if (currentLocationsLevels[action.payload.index]) {
        currentLocationsLevels = [...currentLocationsLevels.slice(0, action.payload.index), action.payload.result.children];
      } else {
        currentLocationsLevels = [...currentLocationsLevels, action.payload.result.children];
      }

      if (currentLocationsLevels[action.payload.index - 1]) {
        currentLocationsLevels[action.payload.index - 1] = currentLocationsLevels[action.payload.index - 1]
          .map((location: ILocation) => {
            return Object.assign({}, location, {
              active: location.id === action.payload.locationID
            });
          });
      }

      return Object.assign({}, state, {
        locationsLevels: currentLocationsLevels
      });

    case SET_LOCATION_FOR_SELECTION:

      return Object.assign({}, state, {
        locationsLevels: state.locationsLevels
          .map((locations: ILocation[]) => locations.map((item: ILocation) => Object.assign({}, item, {
            active: (!item.active) ? item.id === action.payload.locationID : (item.active && !item.hasChildrens) ? false : item.active
          })))
      });

    case SET_SELLER_TYPE:

      return Object.assign({}, state, {
        sellerType: action.payload
      });

    case SET_TITLE:

      return Object.assign({}, state, {
        title: action.payload
      });

    case SET_DESCRIPTION:

      return Object.assign({}, state, {
        description: action.payload
      });

    case SET_PRICE:

      return Object.assign({}, state, {
        price: action.payload
      });

    case SET_YOUTUBE:

      return Object.assign({}, state, {
        youtube: action.payload
      });

    case SET_INCLUDE_LINK:

      return Object.assign({}, state, {
        includeLink: action.payload,
        adPrice: (action.payload) ? state.adPrice + 7.5 : state.adPrice - 7.5
      });

    case SET_WEBSITE:

      return Object.assign({}, state, {
        web: action.payload.link,
        includeWebsite: action.payload.include
      });

    case SET_IMAGE:

      const images = state.images || [];

      return Object.assign({}, state, {
        images: [...images, action.payload]
      });

    case REMOVE_IMAGE:

      return Object.assign({}, state, {
        images: [...state.images.filter((item) => item !== action.payload)]
      });

    case SET_PROMOTION:

      return Object.assign({}, state, {
        promotion: action.payload,
        promotionPrice: calculatePromotionPrice(action.payload)
      });

    case RESET_POSTAD_STATE:

      return Object.assign({}, initialState);

    default:
      return state;
  }
};
