export const CREATE_AD = 'PostAd: create ad';
export const CREATE_AD_SUCCESS = 'PostAd: create ad success';
export const CREATE_AD_REDIRECT = 'PostAd: redirect to ad page';
export const CREATE_AD_FAIL = 'PostAd: create ad fail';

export const LOAD_AD = 'PostAd: load ad';
export const LOAD_AD_SUCCESS = 'PostAd: load ad success';
export const LOAD_AD_FAIL = 'PostAd: load ad fail';

export const CREATE_AD_SEARCH_CATEGORY = 'PostAd: search for category';
export const CREATE_AD_SEARCH_CATEGORY_SUCCESS = 'PostAd: search for category success';
export const CREATE_AD_SEARCH_CATEGORY_FAIL = 'PostAd: search for category fail';

export const CREATE_AD_SEARCH_LOCATION = 'PostAd: search for location';
export const CREATE_AD_SEARCH_LOCATION_SUCCESS = 'PostAd: search for location success';
export const CREATE_AD_SEARCH_LOCATION_FAIL = 'PostAd: search for location fail';

export const CREATE_AD_SEARCH_LOCATION_CLEAR = 'PostAd: clear location suggestions';

export const CREATE_AD_SELECT_CATEGORY = 'PostAd: select ad category';
export const CREATE_AD_CLEAR_CATEGORY = 'PostAd: clear ad category';

export const CREATE_AD_CATEGORY_LOAD_ATTRIBUTES_SUCCESS = 'PostAd: load category attributes success';
export const CREATE_AD_CATEGORY_LOAD_ATTRIBUTES_FAIL = 'PostAd: load category attributes fail';

export const CREATE_AD_SELECT_LOCATION = 'PostAd: select ad location';
export const CREATE_AD_CLEAR_LOCATION = 'PostAd: clear ad location';

export const GET_SUBCATEGORIES_BY_PARENT = 'PostAd: get subcategories by parent id';
export const GET_SUBCATEGORIES_BY_PARENT_SUCCESS = 'PostAd: get subcategories by parent id success';
export const GET_SUBCATEGORIES_BY_PARENT_FAIL = 'PostAd: get subcategories by parent id fail';

export const SET_CATEGORY_FOR_SELECTION = 'PostAd: set category for selection';
export const GET_SUBCATEGORIES_BY_PARENT_RESET = 'PostAd: reset fetched subcategories';

export const REST_LOCATIONS = 'PostAd: reset locations';

export const GET_LOCATIONS_BY_PARENT = 'PostAd: fetch locations by parent';
export const GET_LOCATIONS_BY_PARENT_SUCCESS = 'PostAd: fetch locations by parent success';
export const GET_LOCATIONS_BY_PARENT_FAIL = 'PostAd: fetch locations by parent fail';

export const SET_LOCATION_FOR_SELECTION = 'PostAd: set location for selection';

export const SET_SELLER_TYPE = 'PostAd: set seller type';
export const SET_TITLE = 'PostAd: set title';
export const SET_PRICE = 'PostAd: set price';
export const SET_DESCRIPTION = 'PostAd: set description';
export const SET_INCLUDE_LINK = 'PostAd: set include link';
export const SET_WEBSITE = 'PostAd: set webiste';
export const SET_PROMOTION = 'PostAd: set promotion';
export const SET_YOUTUBE = 'PostAd: set youtube link';
export const SET_IMAGE = 'PostAd: set image';
export const REMOVE_IMAGE = 'PostAd: remove image';

export const RESET_POSTAD_STATE = 'PostAd: reset postad state';
