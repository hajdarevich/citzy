import { Effect, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import {
  CREATE_AD, CREATE_AD_SUCCESS, CREATE_AD_SEARCH_CATEGORY,
  CREATE_AD_SEARCH_CATEGORY_SUCCESS, CREATE_AD_SEARCH_CATEGORY_FAIL, CREATE_AD_REDIRECT,
  GET_SUBCATEGORIES_BY_PARENT_FAIL, GET_SUBCATEGORIES_BY_PARENT_SUCCESS, GET_SUBCATEGORIES_BY_PARENT,
  CREATE_AD_SEARCH_LOCATION, CREATE_AD_SEARCH_LOCATION_SUCCESS, CREATE_AD_SEARCH_LOCATION_FAIL, GET_LOCATIONS_BY_PARENT,
  GET_LOCATIONS_BY_PARENT_SUCCESS, GET_LOCATIONS_BY_PARENT_FAIL, LOAD_AD, LOAD_AD_SUCCESS, CREATE_AD_SELECT_CATEGORY,
  CREATE_AD_CATEGORY_LOAD_ATTRIBUTES_FAIL, CREATE_AD_CATEGORY_LOAD_ATTRIBUTES_SUCCESS
} from './postad.actions';

@Injectable()
export class PostAdEffects {

  @Effect()
  createAd$ = this.actions$
    .ofType(CREATE_AD)
    .debounceTime(1000)
    .switchMap((action: Action) => {

      return Observable.of({
        type: CREATE_AD_SUCCESS,
        payload: {
          id: '4245964f37dd497089f635f907bfdbc7',
          newUser: action.payload.newUser
        }
      });
    });

  @Effect()
  loadAd$ = this.actions$
    .ofType(LOAD_AD)
    .switchMap((action: Action) => {

      return Observable.of({
        type: LOAD_AD_SUCCESS,
        payload: {
          id: '4245964f37dd497089f635f907bfdbc7'
        }
      });
    });

  @Effect()
  categrySearch$ = this.actions$
    .ofType(CREATE_AD_SEARCH_CATEGORY)
    .switchMap((action: Action) => {

      const urlParams = new URLSearchParams();
      urlParams.append('filter', action.payload);
      urlParams.append('maxResultCount', '5');

      return this.http
        .get('http://api.citzy.com/api/services/app/Category/GetSuggest', { search: urlParams })
        .map((response: Response) => response.json())
        .map((categories: {}) => ({ payload: categories, type: CREATE_AD_SEARCH_CATEGORY_SUCCESS }))
        .catch((error: Response) => Observable.of(({ payload: error, type: CREATE_AD_SEARCH_CATEGORY_FAIL })));
    });

  @Effect()
  loadCategoryAttributes$ = this.actions$
    .ofType(CREATE_AD_SELECT_CATEGORY)
    .switchMap((action: Action) => {

      const urlParams = new URLSearchParams();
      urlParams.append('Id', action.payload.id);

      return this.http
        .get('http://api.citzy.com/api/services/app/Category/GetChildren', { search: urlParams })
        .map((response: Response) => response.json())
        .map((attributes: { result: {}}) => ({ payload: attributes.result, type: CREATE_AD_CATEGORY_LOAD_ATTRIBUTES_SUCCESS }))
        .catch((error: Response) => Observable.of(({ payload: error, type: CREATE_AD_CATEGORY_LOAD_ATTRIBUTES_FAIL })));
    });

  @Effect()
  locationSearch$ = this.actions$
    .ofType(CREATE_AD_SEARCH_LOCATION)
    .switchMap((action: Action) => {

      const urlParams = new URLSearchParams();
      urlParams.append('filter', action.payload);
      urlParams.append('maxResultCount', '5');

      return this.http
        .get('http://api.citzy.com/api/services/app/Location/GetSuggest', { search: urlParams })
        .map((response: Response) => response.json())
        .map((categories: {}) => ({ payload: categories, type: CREATE_AD_SEARCH_LOCATION_SUCCESS }))
        .catch((error: Response) => Observable.of(({ payload: error, type: CREATE_AD_SEARCH_LOCATION_FAIL })));
    });

  @Effect()
  categryGetByParentID$ = this.actions$
    .ofType(GET_SUBCATEGORIES_BY_PARENT)
    .mergeMap((action: Action) => {

      const urlParams = new URLSearchParams();
      urlParams.append('Id', action.payload.categoryID);

      return this.http
        .get('http://api.citzy.com/api/services/app/Category/GetChildren', { search: urlParams })
        .map((response: Response) => response.json())
        .map((categories: {}) => {
          return ({
            type: GET_SUBCATEGORIES_BY_PARENT_SUCCESS,
            payload: Object.assign(categories, {
              index: action.payload.index,
              categoryID: action.payload.categoryID
            })
          });
        })
        .catch((error: Response) => Observable.of(({ payload: error, type: GET_SUBCATEGORIES_BY_PARENT_FAIL })));
    });

  @Effect()
  locationGetByParentID$ = this.actions$
    .ofType(GET_LOCATIONS_BY_PARENT)
    .switchMap((action: Action) => {

      const urlParams = new URLSearchParams();
      urlParams.append('Id', action.payload.locationID);

      return this.http
        .get('http://api.citzy.com/api/services/app/Location/GetChildren', { search: urlParams })
        .map((response: Response) => response.json())
        .map((locations: {}) => {
          return ({
            type: GET_LOCATIONS_BY_PARENT_SUCCESS,
            payload: Object.assign(locations, {
              index: action.payload.index,
              locationID: action.payload.locationID
            })
          });
        })
        .catch((error: Response) => Observable.of(({ payload: error, type: GET_LOCATIONS_BY_PARENT_FAIL })));
    });

  @Effect()
  redirectAfterAdCreation$ = this.actions$
    .ofType(CREATE_AD_SUCCESS)
    .switchMap((action: Action) => {

      this.router.navigateByUrl(`/postad/${action.payload.id}`);
      return Observable.of(({ type: CREATE_AD_REDIRECT }));
    });

  constructor(private actions$: Actions, private http: Http, private router: Router) {}
}
