import { Action, ActionReducer } from '@ngrx/store';
import { GET_RECOMMENDED_CARDS_SUCCESS } from './cards.actions';

export interface IRecommendedCards {
  image: string;
  name: string;
  description: string;
  price: number;
}

export interface ICards {
  areRecommendedCardsLoaded: boolean;
  recommended: IRecommendedCards[];
}

const initialState: ICards = {
  areRecommendedCardsLoaded: false,
  recommended: [],
};

export const cardsReducer: ActionReducer<ICards> = (state: ICards = initialState, action: Action): ICards => {

  switch (action.type) {

    case GET_RECOMMENDED_CARDS_SUCCESS:

      return Object.assign({}, state, {
        recommended: action.payload,
        areRecommendedCardsLoaded: true
      });

    default:
      return state;
  }
};
