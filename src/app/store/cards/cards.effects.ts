import { Effect, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';

import { GET_RECOMMENDED_CARDS_SUCCESS } from './cards.actions';
import { GET_RANDOM_BANNER_SUCCESS } from '../layout/layout.actions';

@Injectable()
export class CardsEffects {

  @Effect()
  getRecommendedCards$ = this.actions$
    .ofType(GET_RANDOM_BANNER_SUCCESS)
    .map(() => {
      return ({ payload: [
        {
          image: 'assets/images/cards-samples/image1.jpg',
          name: 'Urgently Wanted Actors, Models, Extras - TV adverts & Pop Videos. BEGINNERS WELCOME!!!',
          description: 'Das grosse kvalitet',
          price: 7000
        },
        {
          image: 'assets/images/cards-samples/image2.jpg',
          name: 'Peugeot 206',
          description: 'France car',
          price: 1900
        },
        {
          image: 'assets/images/cards-samples/image5.JPG',
          name: 'Peugeot 206',
          description: 'I am looking for a for ever home for my loving 2 year old Dogue de Bordeau.',
          price: 1900
        },
        {
          image: 'assets/images/cards-samples/image6.jpg',
          name: '207 BMW',
          description: '2007 BMW 320i MSPORT 50K low mileage FSH LE MANS BLUE 330 335 replica',
          price: 3200
        },
        {
          image: 'assets/images/cards-samples/image7.jpg',
          name: 'Samsung s7 edge 32gb',
          description: 'unlocked cheap',
          price: 180
        },
        {
          image: 'assets/images/cards-samples/image8.jpg',
          name: 'Vauxhall Insignia',
          description: ' 2.0CDTi 16v SRi VX-Line',
          price: 6795
        },
        {
          image: 'assets/images/cards-samples/image3.jpg',
          name: 'Stadium Grbavica',
          description: 'Reconstruction',
          price: 200000
        },
        {
          image: 'assets/images/cards-samples/image4.jpg',
          name: 'Stadium JNA',
          description: 'Partizan Belgrade Stadium',
          price: 101562678
        },
        {
          image: 'assets/images/cards-samples/image9.jpg',
          name: 'Fifa 17 (Ps4) - New , Sealed Cd.',
          description: 'Used',
          price: 20
        },
        {
          image: 'assets/images/cards-samples/image15.jpg',
          name: 'Oct 2010 LCI (FACELIFT) BMW M3 4.0 V8 MANUAL 420bhp, 74K! Top Spec! Pro Nav! STUNNING CAR! FBMWSH!',
          description: 'Dunmurry, Belfast',
          price: 22850
        },
        {
          image: 'assets/images/cards-samples/image16.jpg',
          name: '2014 BMW M4 CONVERTIBLE 3.0 DCT CONVERTIBLE PETROL',
          description: 'Hartley, Kent',
          price: 44500
        },
        {
          image: 'assets/images/cards-samples/image17.jpg',
          name: '2012 RANGE ROVER SPORT 3.0 SDV6 HSE AUTO WITH LUXURY PACK. LOADED TOP SPEC !!',
          description: 'Hartley, Kent',
          price: 26990
        }
      ], type: GET_RECOMMENDED_CARDS_SUCCESS });
    });

  constructor(private actions$: Actions) {
  }
}
