import { combineReducers, ActionReducer, Action, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { compose } from '@ngrx/core';
import { storeFreeze } from 'ngrx-store-freeze';
import { CommonModule } from '@angular/common';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { profileReducer, IProfile } from './profile/profile.reducer';
import { ProfileEffects } from './profile/profile.effects';
import { environment } from '../../environments/environment';
import { layoutReducer, ILayout } from './layout/layout.reducer';
import { LayoutEffects } from './layout/layout.effects';
import { Iad, postadReducer } from './postad/postad.reducer';
import { PostAdEffects } from './postad/postad.effects';
import { ICards, cardsReducer } from './cards/cards.reducer';
import { CardsEffects } from './cards/cards.effects';


// all new reducers should be define here
export interface IAppState {
  layout: ILayout;
  cards: ICards;
  post: Iad;
  profile: IProfile;
}

// all new reducers should be define here
const reducers = {
  postad: postadReducer,
  cards: cardsReducer,
  layout: layoutReducer,
  profile: profileReducer
};

const productionReducer: ActionReducer<IAppState> = combineReducers(reducers);
const developmentReducer: ActionReducer<IAppState> = compose(storeFreeze, combineReducers)(reducers);

export function reducer(state: IAppState, action: Action) {
  if (environment.production) {
    return productionReducer(state, action);
  } else {
    return developmentReducer(state, action);
  }
}

@NgModule()
export class DummyModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CommonModule
    };
  }
}

export const store: ModuleWithProviders = StoreModule.provideStore(reducer);
export const i: ModuleWithProviders = (!environment.production) ? StoreDevtoolsModule.instrumentOnlyWithExtension() : DummyModule.forRoot();

export const effects: ModuleWithProviders[] = [
  EffectsModule.run(ProfileEffects),
  EffectsModule.run(LayoutEffects),
  EffectsModule.run(PostAdEffects),
  EffectsModule.run(CardsEffects)
];
