import { Effect, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Http, URLSearchParams, Response } from '@angular/http';

@Injectable()
export class ProfileEffects {
  constructor(private actions$: Actions, private http: Http, private router: Router) {}
}
