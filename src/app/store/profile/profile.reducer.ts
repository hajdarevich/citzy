import { Action, ActionReducer } from '@ngrx/store';
import { RESET_PROFILE_STATE } from './profile.actions';

export interface IProfile {}

export const initialState: IProfile = {};

export const profileReducer: ActionReducer<IProfile> = (state: IProfile = initialState, action: Action): IProfile => {

  switch (action.type) {

    case RESET_PROFILE_STATE:

      return Object.assign({}, initialState);

    default:
      return state;
  }
};
