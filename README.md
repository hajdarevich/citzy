## Citzy

- Angular 2
- Semantic UI
- Webpack ( angular-cli )

## Concepts

 - Angular 2 ( 2.x )
 - Webpack ( angular-cli )
 - Semantic UI
 - Redux
 - Smart & dumb components
 - AOT: Ahead-of-Time compilation
 - Advanced routing ( lazy loading, router outlets...)
 - Feature based project structure
 - Solution align with: https://angular.io/styleguide
 - Localization support, i18n https://angular.io/docs/ts/latest/cookbook/i18n.html ( LTR and RTL )
 - Deploy-able client.
 - Responsive and optimized solution.

## Install / Development

```bash
git clone ...

# Install dependencies
npm install

# start server
npm run start
```

Install Redux DevTools chrome extenstion:

https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd

## Build Requirements

- NodeJS LTS ( 6.10.x )

```bash
npm build
```

- Deploy everything from **dist/** to app server.

## Rules

- Each component must have test component in same directory ( unit tested ).
- Before each push, code must be compiled without errors and linted without warnings.
- Do not push half done functionalities.

